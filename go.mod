module gitlab.com/vbuser2004/morty

go 1.14

require (
	gitlab.com/vbuser2004/fasthttp v1.14.0
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/text v0.3.3
)
